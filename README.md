# Small XML accessor library

I wanted a small, lightweight XML accessor library with
a practical subset of XPath.
My XML inputs are well-formed, UTF-8, and small enough to be
loaded entirely into memory.

## Overview

Describe a region of UTF-8 XML using this type:

```c
struct xml { const char *s; size_t len; };

#define XML_INIT (struct xml) { 0, 0 }
```

These library functions access the XML:

    struct xml xml_root(struct xml doc);
    struct xml xml_content(struct xml tag);
    struct xml xml_at(struct xml xml, const char *path);
    bool xml_next_tag(struct xml xml, struct xml *tag_iter);
    bool xml_next_attr(struct xml tag, struct xml *attr_iter);
    bool xml_name_eq(struct xml tag, const char *name);
    bool xml_attr_eq(struct xml tag, const char *attr, const char *value);
    char *xml_get_name(struct xml tag);
    char *xml_get_attr(struct xml tag, const char *name, size_t *len_return);
    char *xml_get_text(struct xml xml, size_t *len_return);
    char *xml_get_at(struct xml xml, const char *path, size_t *len_return);
    char *xml_attr_get_name(struct xml attr);
    char *xml_attr_get_value(struct xml attr, size_t *len_return);

Functions that return `char *` return heap-allocated, NUL-terminated,
XML-decoded UTF-8 strings.
An optional `size_t *len_return` can be supplied when the returned
string could contain NUL.

Features:

* Subset of XPath allows quick access to attribute and text values
* Iterator functions allow allocation-free traversal of XML document
* Standard CRLF to LF conversion

## Example

```c
#include <stdio.h>
#include <string.h>
#include "xml.h"

int main ()
{
    char buf[4096];
    size_t buflen = 0;

    /* Read the XML file into memory */
    FILE *f = fopen("example.xml", "r");
    if (f) {
        buflen = fread(buf, 1, sizeof buf, f);
        fclose(f);
    }

    /* Construct a whole-document span */
    struct xml doc = { buf, buflen };

    /* Compute the span between <employees> and </employees>.
     * If we had used "employees" without the trailing '/'
     * then we would get the span including the opening
     * and closing <employees> tags. */
    struct xml employees_content = xml_at(doc, "employees/");

    /* Iterating over the toplevel tags in employees_content */
    for (struct xml emp = XML_INIT;
         xml_next_tag(employees_content, &emp);)
    {

#ifdef DEBUG
	fprintf(stderr, "emp=(%.*s)\n", (int)emp.len, emp.s);
#endif

	/* Access the text content of the name element. */
        char *name = xml_get_at(emp, "employee/name", 0);
        if (name && strcmp(name, "John Smith") == 0) {
	    /* Access the text content of the year attribute.
	     * This demonstrates receiving the length, although
	     * the returned string will always be NUL-terminated. */
            size_t yearlen;
            char *year = xml_get_at(emp,
                          "employee/birth/date@year", &yearlen);
            if (year)
                printf("John was born in %.*s\n", (int)yearlen, year);
            free(year);
        }
        free(name);
    }
}
```

## Not supported

The following are not supported:

* Encodings other than UTF-8 (especially UTF-16)
* Processing instructions (they are ignored like comments)
* `<!ENTITY>` `<!DOCTYPE>` `<!ELEMENT>` `<!ATTLIST>`
* Strict whitespace rejection (x0-x20 are accepted as whitespace)
* Strict NMTOKEN rejection (any byte over x20 other than `> / =` is accepted)
* UTF-8 normalization (the input is usually treated as bytes)
* Namespaces

The following common XML tolerances are not supported:

* `<` in attribute values: `&lt;` *must* be used instead of `<`.
* omitted `;` in entities: entities *must* be terminated by `;`
* unquoted attribute values: attribute values *must* be quoted with `"` or `'`
* whitespace after `<`

## Author

David Leonard
