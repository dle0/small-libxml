#include <stdio.h>
#include <string.h>
#include "xml.h"

int main ()
{
    char buf[4096];
    size_t buflen = 0;

    /* Read the XML file into memory */
    FILE *f = fopen("example.xml", "r");
    if (f) {
        buflen = fread(buf, 1, sizeof buf, f);
        fclose(f);
    }

    /* Construct a whole-document span */
    struct xml doc = { buf, buflen };

    /* Compute the span between <employees> and </employees>.
     * If we had used "employees" without the trailing '/'
     * then we would get the span including the opening
     * and closing <employees> tags. */
    struct xml employees_content = xml_at(doc, "employees/");

    /* Iterating over the toplevel tags in employees_content */
    for (struct xml emp = XML_INIT;
         xml_next_tag(employees_content, &emp);)
    {

#ifdef DEBUG
	fprintf(stderr, "emp=(%.*s)\n", (int)emp.len, emp.s);
#endif

	/* Access the text content of the name element. */
        char *name = xml_get_at(emp, "employee/name", 0);
        if (name && strcmp(name, "John Smith") == 0) {
	    /* Access the text content of the year attribute.
	     * This demonstrates receiving the length, although
	     * the returned string will always be NUL-terminated. */
            size_t yearlen;
            char *year = xml_get_at(emp,
                          "employee/birth/date@year", &yearlen);
            if (year)
                printf("John was born in %.*s\n", (int)yearlen, year);
            free(year);
        }
        free(name);
    }
}

