#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include "xml.h"

/* Unit tests for xml.c */

#define XML(str) (struct xml){.s = (str), .len = sizeof (str) - 1}

/* Tests if an XML span ends with expected s */
static bool ends_with(struct xml x, const char *s) {
	int slen = strlen(s);
	return x.len >= slen &&
	       memcmp(x.s + x.len - slen, s, slen) == 0;
}

/* Tests if an XML span has expected content */
static bool xml_eq(struct xml a, struct xml b) {
	return a.len == b.len && memcmp(a.s, b.s, a.len) == 0;
}

/* Convert data to a C string, used for error printing */
static const char *c_str(const char *p, size_t n) {
	if (!p) return "NULL";
	static char buf[1024];
	char *b = buf;
	*b++ = '"';
	while (n--) {
		unsigned char c = *p++;
		switch (c) {
		case '\n': *b++ = '\\'; *b++ = 'n'; break;
		case '\r': *b++ = '\\'; *b++ = 'r'; break;
		case '\t': *b++ = '\\'; *b++ = 't'; break;
		case '\0': *b++ = '\\'; *b++ = '0'; break;
		case '\\': *b++ = '\\'; *b++ = '\\'; break;
		case '\"': *b++ = '\\'; *b++ = '"'; break;
		default:
		    if (c <= 0x1f || c >= 0x7f) {
			const char hex[] = "0123456789abcdef";
			*b++ = '\\'; *b++ = 'x';
			*b++ = hex[c >> 4 & 0xf];
			*b++ = hex[c >> 0 & 0xf];
		    } else
		        *b++ = c;
		}
	}
	*b++ = '"';
	*b = 0;
	return buf;
}

/* Assert that two XML expressions have equal content.
 * On assertion failure, displays detailed error and aborts. */
#define assert_xml_eq(e1, e2) do { \
	struct xml _e1 = (e1); \
	struct xml _e2 = (e2); \
	if (!xml_eq(_e1, _e2)) { \
		fprintf(stderr, "%s:%d: assertion failed %s != %s\n", \
				__FILE__, __LINE__, #e1, #e2); \
		fprintf(stderr, "\tact = %s\n", c_str(_e1.s, _e1.len)); \
		fprintf(stderr, "\texp = %s\n", c_str(_e2.s, _e2.len)); \
		abort(); \
	} \
    } while (0)

/* Asserts that string e1 of length len is equal to string literal e2.
 * The len is needed because e2 can contain NULs.
 * Also frees(e1) */
#define assert_eq_free(e1, len, e2) do { \
	char *_e1 = (e1); \
	const char _e2[] = e2; \
	if ((len) != sizeof _e2 -1 ||\
	    !_e1 || \
	    memcmp(_e1, _e2, sizeof _e2) != 0) \
	{ \
		fprintf(stderr, "%s:%d: assertion failed %s != %s\n", \
			__FILE__, __LINE__, #e1, #e2); \
		fprintf(stderr, "\tact = %s (%u)\n", \
			c_str(_e1, len), (unsigned)(len)); \
		fprintf(stderr, "\texp = %s (%u)\n", \
			c_str(_e2, sizeof _e2 - 1), (unsigned)sizeof _e2 - 1); \
		abort(); \
	} \
	free(_e1); \
    } while (0)

int
main() {
	/* xml_root() */
	assert_xml_eq(xml_root(XML("<a/>")), XML("<a/>"));
	assert_xml_eq(xml_root(XML("<?xml version='1.0'?>\n"
	                           "<doc>hello</doc>\n")),
		      XML("<doc>hello</doc>"));
	assert_xml_eq(xml_root(XML("<?xml?>\n")), XML(""));

	/* xml_content() */
	assert_xml_eq(xml_content(XML("<a>foo</a>")), XML("foo"));
	assert_xml_eq(xml_content(XML("<a></a>")), XML(""));
	assert_xml_eq(xml_content(XML("<a>foo</a>bar")), XML("foo"));
	assert_xml_eq(xml_content(XML("<a/>")), XML(""));
	assert_xml_eq(xml_content(XML("<a/>foo")), XML(""));
	assert_xml_eq(xml_content(XML("<a b='c'>foo</a>bar")), XML("foo"));
	assert_xml_eq(xml_content(XML("<a b='c' d = \"e\" >foo</a>bar")), XML("foo"));
	assert_xml_eq(xml_content(XML("<a b='c' d = \"e\" />")), XML(""));
	assert_xml_eq(xml_content(XML(
		 "<a><b></b><c/>.<d><?e?><f/><!--g--></d></a>xyz")),
		XML("<b></b><c/>.<d><?e?><f/><!--g--></d>"));

	/* xml_name_eq() */
	assert(xml_name_eq(XML("<a>"), "a"));
	assert(xml_name_eq(XML("<a/>"), "a"));
	assert(xml_name_eq(XML("<a b='c'>"), "a"));
	assert(!xml_name_eq(XML("<a/>"), ""));
	assert(!xml_name_eq(XML("<a/>"), "aa"));

	/* xml_get_name() */
	size_t len;
	assert_eq_free(xml_get_name(XML("<a>")), 1, "a");
	assert_eq_free(xml_get_name(XML("<abc d='ef'>")), 3, "abc");

	/* xml_get_attr() */
	struct xml x = XML("<tag \n"
	                   " single = 'abc'"
			   " double=\"def\""
			   " empty = ''"
			   " entity=\"&quot;+&apos;&#65;&#x22;\""
			   "/>");
	assert(!xml_get_attr(x, "wrong", &len));
	assert(!xml_get_attr(x, "wrong", NULL));
	assert(!xml_get_attr(x, "", &len));
	assert(!xml_get_attr(x, "", NULL));
	assert_eq_free(xml_get_attr(x, "single", NULL), 3, "abc");
	len = 0;
	assert_eq_free(xml_get_attr(x, "double", &len), len, "def");
	assert_eq_free(xml_get_attr(x, "entity", &len), len, "\"+'A\"");
	assert_eq_free(xml_get_attr(x, "empty", &len), len, "");

	/* Various UTF-8 and entities in attributes are OK */
	x = XML("<tag a='&lt;&amp;&gt;\xe2\x94\xa4&#x37e;!'>");
	assert_eq_free(xml_get_attr(x, "a", &len), len,
		"<&>\xe2\x94\xa4\xcd\xbe!");

	/* NULs in attributes are OK */
	x = XML("<tag a='nul \0 and &#0; ok'>");
	assert_eq_free(xml_get_attr(x, "a", &len), len,
		"nul \0 and \0 ok");

	/* CRLF and CR are translated into LF within attributes */
	x = XML("<tag\r\na\n=\r'new\r\nlines\nok\r'\r>");
	assert_eq_free(xml_get_attr(x, "a", &len), len,
		"new\nlines\nok\n");

	/* xml_attr_eq() */
	assert(!xml_attr_eq(XML("<tag/>"), "noexist", ""));
	assert(!xml_attr_eq(XML("<tag/>"), "noexist", "noexist"));
	assert(xml_attr_eq(XML("<tag/>"), "noexist", NULL));

	assert(xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "a", "1"));
	assert(xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "b", "2"));
	assert(xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "c", "3"));
	assert(xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "d", NULL));

	assert(!xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "a", "11"));
	assert(!xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "b", ""));
	assert(!xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "c", " 3"));
	assert(!xml_attr_eq(XML("<tag a='1' b='2' c='3'/>"), "d", ""));

	assert(xml_attr_eq(XML("<tag a='b&lt;c'/>"), "a", "b<c"));
	assert(xml_attr_eq(XML("<tag a='new\r\nline'/>"), "a", "new\nline"));

	/* xml_get_text() */
	assert_eq_free(xml_get_text(XML(""), &len), len, "");
	assert_eq_free(xml_get_text(XML("foo"), &len), len, "foo");
	assert_eq_free(xml_get_text(XML("foo\r\nbar\nbaz\r"), &len), len,
		"foo\nbar\nbaz\n");
	assert_eq_free(xml_get_text(XML("nul\0and &#0; ok"), &len), len,
		"nul\0and \0 ok");
	assert_eq_free(xml_get_text(XML("<?xml?>\n<b>bold</b> and fun<br/>!"),
		&len), len, "\nbold and fun!");
	assert_eq_free(xml_get_text(
		XML("any <!-- comment -- allowed --><!---->!"), &len), len,
		"any !");
	assert_eq_free(xml_get_text(
		XML("cdata <![CDATA[<ok> &amp;]]> &amp; good"), &len), len,
		"cdata <ok> &amp; & good");

	/* xml_next_tag() */
	struct xml recipe_doc = XML(
		"<?xml version=\"1.0\"?>\n"
		"<recipe name='breakfast'>\n"
		"  <ingredients>\n"
		"    <!-- Keep it classy -->\n"
		"    <ingredient quantity='2'>egg</ingredient>\n"
		"    <ingredient quantity='5'>bacon rasher</ingredient>\n"
		"    <ingredient quantity='2'>bread slice</ingredient>\n"
		"    <ingredient quantity='50 g'>salmon</ingredient>\n"
		"    <ingredient quantity='100 ml'>Hollandaise sauce</ingredient>\n"
		"    <ingredient quantity='350 ml'>caf\xc3\xa9</ingredient>\n"
		"  </ingredients>\n"
		"  <method>\n"
		"    <step>Blend all. Consume without pausing.</step>\n"
		"  </method>\n"
		"</recipe>\n"
		"<!-- Enjoy responsibly -->\n"
		);
	struct xml i;

	/* Iterating over the recipe_doc finds just one element */
	i = XML_INIT;
	assert(xml_next_tag(recipe_doc, &i));
	assert(xml_name_eq(i, "recipe"));
	assert(ends_with(i, "</recipe>"));
	assert(!xml_next_tag(recipe_doc, &i));
	assert(!i.s && !i.len);

	/* Iterating over the <recipe> content finds two elements */
	struct xml recipe = xml_content(xml_root(recipe_doc));
	i = XML_INIT;
	assert(xml_next_tag(recipe, &i));
	assert(xml_name_eq(i, "ingredients"));
	struct xml ingredients = xml_content(i); /* save for next test */
	assert(xml_next_tag(recipe, &i));
	assert(xml_name_eq(i, "method"));
	assert(!xml_next_tag(recipe, &i));
	assert(!i.s && !i.len);

	/* Iterating over the content of <ingredients> works */
	i = XML_INIT;
	assert(xml_next_tag(ingredients, &i));
	assert(xml_name_eq(i, "ingredient"));
	assert_eq_free(xml_get_text(i, &len), len, "egg");

	assert(xml_next_tag(ingredients, &i));
	assert(xml_name_eq(i, "ingredient"));
	assert_eq_free(xml_get_text(i, &len), len, "bacon rasher");
	assert_eq_free(xml_get_attr(i, "quantity", &len), len, "5");

	assert(xml_next_tag(ingredients, &i));
	assert_eq_free(xml_get_text(i, &len), len, "bread slice");

	assert(xml_next_tag(ingredients, &i));
	assert_eq_free(xml_get_text(i, &len), len, "salmon");
	assert(xml_next_tag(ingredients, &i));
	assert(xml_next_tag(ingredients, &i));
#if (__STDC_VERSION__ +0 >= 201112L)
	assert_eq_free(xml_get_text(i, &len), len, u8"café");
#else
	assert_eq_free(xml_get_text(i, &len), len, "caf\xc3\xa9");
#endif
	assert(!xml_next_tag(ingredients, &i));
	assert(!i.s && !i.len);

	/* xml_at() */
	assert_xml_eq(xml_at(XML("<a/>"), "a"), XML("<a/>"));
	assert_xml_eq(xml_at(XML("<a/>"), "/a"), XML("<a/>"));
	assert_xml_eq(xml_at(XML("<a/>"), "a/"), XML(""));
	assert_xml_eq(xml_at(XML("<a/>"), "a/b"), XML_INIT);
	assert_xml_eq(xml_at(XML("<a/>"), "b"), XML_INIT);
	assert_xml_eq(xml_at(XML("<a/>"), "/b"), XML_INIT);

	/* xml_at() trailing slash */
	assert_xml_eq(xml_at(XML("<a><b><c/></b></a>"), "a/b/c"),
				     XML("<c/>"));
	assert_xml_eq(xml_at(XML("<a><b><c/></b></a>"), "a/"),
				  XML("<b><c/></b>"));

	/* xml_at() indicies */
	struct xml a3 = XML(" <a id='1'/> <a id='2'/> <b/> <a id='3'/> ");
	assert_xml_eq(xml_at(a3, "a"), XML("<a id='1'/>"));
	assert_xml_eq(xml_at(a3, "a[1]"), XML("<a id='1'/>"));
	assert_xml_eq(xml_at(a3, "a[2]"), XML("<a id='2'/>"));
	assert_xml_eq(xml_at(a3, "b"), XML("<b/>"));
	assert_xml_eq(xml_at(a3, "a[3]"), XML("<a id='3'/>"));

	assert_xml_eq(xml_at(XML("<a b='c'>"), "a@b"), XML("c"));
	assert_xml_eq(xml_at(XML("<a b='c'>"), "a@c"), XML_INIT);

	/* xml_get_at() is the same but mallocs & NULL means not found */
	assert_eq_free(xml_get_at(recipe_doc,
		"recipe/ingredients/ingredient[6]@quantity", &len),
		len, "350 ml");
	assert_eq_free(xml_get_at(recipe_doc, "recipe@name", NULL),
		strlen("breakfast"), "breakfast");
	assert(!xml_get_at(recipe_doc, "noexist/", &len));
	assert(!xml_get_at(recipe_doc, "noexist/", NULL));
	assert(!xml_get_at(recipe_doc, "recipe@noexist", &len));
	assert(!xml_get_at(recipe_doc, "recipe@noexist", NULL));

	/* xml_next_attr() over empty attribute list */
	struct xml ai;
	ai = XML_INIT;
	assert(!xml_next_attr(XML("<tag/>"), &ai));
	assert(!ai.s && !ai.len);

	/* xml_attr_get_name() and xml_attr_get_value() */
	struct xml adoc = XML("<tag a='1' b\r=\"2\" c = '&gt;3&apos;'></tag>");
	ai = XML_INIT;
	assert(xml_next_attr(adoc, &ai));
	assert_xml_eq(ai, XML("a='1'"));
	assert_eq_free(xml_attr_get_name(ai), 1, "a");
	assert_eq_free(xml_attr_get_value(ai, &len), len, "1");

	assert(xml_next_attr(adoc, &ai));
	assert_xml_eq(ai, XML("b\r=\"2\""));
	assert_eq_free(xml_attr_get_name(ai), 1, "b");
	assert_eq_free(xml_attr_get_value(ai, &len), len, "2");

	assert(xml_next_attr(adoc, &ai));
	assert_xml_eq(ai, XML("c = '&gt;3&apos;'"));
	assert_eq_free(xml_attr_get_name(ai), 1, "c");
	assert_eq_free(xml_attr_get_value(ai, &len), len, ">3'");
}
