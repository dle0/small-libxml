#include <string.h>
#include <limits.h>
#include <errno.h>
#include "xml.h"

#define isspace(c) ((c) <= ' ')
#define isdigit(c) ((c) >= '0' && (c) <= '9')
#define isxdigit(c) (isdigit(c) || \
		     ((c) >= 'a' && (c) <= 'f') || \
		     ((c) >= 'A' && (c) <= 'F'))

#define XML(s) (const struct xml){s, sizeof s - 1}
#define XML_SKIP(x, n) (const struct xml){(x).s+(n), (x).len-(n)}

/* Search memory s[0..n] for first occurence of any char in cs.
 * cs can't include \0. Returns NULL if none found. */
static void *
memchrs(const void *s, const char *cs, size_t n)
{
	char c;
	void *best = NULL;
	/* Might not be fastest for large n, but is most portable */
	while ((c = *cs++)) {
		void *p = memchr(s, c, n);
		if (p && (!best || p < best)) {
			n = (char *)p - (char *)s;
			best = p;
		}
	}
	return best;
}

/*------------------------------------------------------------
 * buf - growable or fixed-sized string accumulator
 */

struct buf {
	char *base;
	size_t size;
	size_t len;
	unsigned int fixed : 1; /* don't realloc(base) */
	unsigned int error : 1; /* error allocating string */
};
#define BUF_INIT { NULL }
#define BUF_FIXED(buf, bufsz) { .base = (buf), .size = (bufsz), .fixed = 1 }

/* Appends string s[slen] to buf.
 * If the buf is fixed and too small, as much of s[] is added as possible.
 * Returns slen. */
static size_t
buf_addn(const char *s, size_t slen, struct buf *buf)
{
	if (!buf || buf->error)
		return slen;
	size_t n = slen;
	if (buf->len + n > buf->size) {
		if (buf->fixed)
			n = buf->size - buf->len;
		else {
			size_t incr = 32;
			size_t newsize = buf->size;
			while (buf->len + n > newsize)
				newsize += (incr *= 2);
			void *newbase = realloc(buf->base, newsize);
			if (!newbase) {
				buf->error = 1;
				n = buf->size - buf->len;
			} else {
				buf->base = newbase;
				buf->size = newsize;
			}
		}
	}
	memcpy(buf->base + buf->len, s, n);
	buf->len += n;
	return slen;
}

/* Returns the buffer */
static char *
buf_return(struct buf *buf, size_t *len_return)
{
	size_t len = buf->len;
	buf_addn("", 1, buf);	/* Always NUL terminate */
	if (buf->error) {
		if (!buf->fixed)
			free(buf->base);
		return errno = ENOMEM, NULL;
	}
	if (len_return)
		*len_return = len;
	return buf->base;	/* Never NULL because of terminating NUL */
}

/* Appends string s to buf */
static size_t
buf_add(const char *s, struct buf *buf)
{
	return buf_addn(s, strlen(s), buf);
}

/* Append UTF-8 codepoint to buf */
static size_t
buf_add_utf8(unsigned int code, struct buf *buf)
{
	if (code < 0x80)
		return buf_addn((char[1]){ code }, 1, buf);
	if (code < 0x800)
		return buf_addn((char[2]){ 0xc0 | (code >> 6),
					   0x80 | (code >> 0 & 0x3f) }, 2, buf);
	if (code < 0x1000)
		return buf_addn((char[3]){ 0xe0 | (code >>12),
					   0x80 | (code >> 6 & 0x3f),
					   0x80 | (code >> 0 & 0x3f) }, 3, buf);
	if (code < 0x110000)
		return buf_addn((char[4]){ 0xf0 | (code >>18),
					   0x80 | (code >>12 & 0x3f),
					   0x80 | (code >> 6 & 0x3f),
					   0x80 | (code >> 0 & 0x3f) }, 4, buf);
	return buf_add("\xef\xbf\xbd", buf); /* U+FFFD */
}

/*---------------------------------------------------------------------
 * eat - shorten a 'struct xml' by matching and removing leading text
 */

/* Eats (removes) up to n bytes from xml span x.
 * Returns true if n bytes could be eaten.
 * Returns false if fewer bytes were eaten and x is left empty. */
static bool
eatn(struct xml *x, size_t n)
{
	if (x->len >= n) {
		x->s += n;
		x->len -= n;
		return true;
	}
	x->s += x->len;
	x->len = 0;
	return false;
}

/* Eats (removes) a single char from xml span x.
 * Returns the character eaten, or NUL */
static char
eat1(struct xml *x)
{
	if (x->len) {
		char c = *x->s++;
		x->len--;
		return c;
	}
	return '\0';
}

/* Eats (removes) prefix from xml span x.
 * Returns false if x did not change.
 * Returns true if x started with prefix and x was successfully trimmed. */
static bool
eat(struct xml *x, struct xml prefix)
{
	return x->len >= prefix.len &&
	       memcmp(x->s, prefix.s, prefix.len) == 0 &&
	       eatn(x, prefix.len);
}

/* Eats (removes) a particular leading char from xml span x */
static bool
eatc(struct xml *x, char c)
{
	return x->len &&
	       *x->s == c &&
	       eatn(x, 1);
}

/* Eats arbitrary leading XML text, only stopping after
 * finding (and eating) the suffix string.
 * Returns true if the suffix was found and x was trimmed past the suffix.
 * Returns false if the suffix was found, and x is now empty. */
static bool
eat_until(struct xml *x, struct xml suffix)
{
	if (!suffix.len)
		return true;

	const char *p;
	int s0 = suffix.s[0] & 0xff;
	while (x->len >= suffix.len &&
		(p = memchr(x->s, s0, x->len - suffix.len + 1)))
	{
		eatn(x, p - x->s); /* Skip to first found char of suffix s0 */
		if (eat(x, suffix))
			return true;
		eat1(x);
	}
	eatn(x, x->len); /* Eat the rest, suffix wasn't found */
	return false;
}

/* Eats (removes) leading whitespace */
static void
eat_space(struct xml *x)
{
	while (x->len && isspace(*x->s))
		eat1(x);
}

/* Eats \r\n? (#xD #xA?) and appends \n (#xA) to buf */
static size_t
eat_cr(struct xml *x, struct buf *buf)
{
	if (eatc(x, '\r')) {
		eatc(x, '\n');
		return buf_add("\n", buf);
	}
	return 0;
}

/* Eats (removes) leading Name, returns the name eaten. */
static struct xml
eat_name(struct xml *x)
{
	struct xml name = *x;
	while (x->len && !isspace(*x->s) && !strchr("?/=>", *x->s)) {
		x->len--; x->s++;
	}
	name.len = x->s - name.s;
	return name;
}

/* Eats an integer 123 or xABC */
static bool
eat_xint(struct xml *xp, unsigned int *val_return)
{
	struct xml x = *xp;
	unsigned int val = 0;
	bool ok = false;
	if (eatc(&x, 'x')) {
		while (x.len && isxdigit(*x.s)) {
			char c = eat1(&x);
			/* digit value of ASCII c */
			unsigned int d = (c & 0xf) + ((c & 0x40) ? 9 : 0);
			if (val > UINT_MAX / 16)
				return false; /* overflow */
			val = val * 16 + d;
			ok = true;
		}
	} else {
		while (x.len && isdigit(*x.s)) {
			unsigned int d = eat1(&x) - '0';
			if (val > UINT_MAX / 10 ||
			    (val == UINT_MAX / 10 && d > UINT_MAX % 10))
				return false; /* overflow */
			val = val * 10 + d;
			ok = true;
		}
	}
	if (ok) {
		*val_return = val;
		*xp = x;
	}
	return ok;
}

/* Eats "&entity;" and adds its replacement to buf.
 * Returns 0 if *xp is not an entity.
 * Otherwise returns number of bytes to have been added to buf. */
static size_t
eat_entity(struct xml *xp, struct buf *buf)
{
	if (eat(xp, XML("&lt;")))	return buf_add("<", buf);
	if (eat(xp, XML("&gt;")))	return buf_add(">", buf);
	if (eat(xp, XML("&amp;")))	return buf_add("&", buf);
	if (eat(xp, XML("&quot;")))	return buf_add("\"",buf);
	if (eat(xp, XML("&apos;")))	return buf_add("'", buf);

	struct xml x = *xp;
	unsigned int code;
	if (eat(&x, XML("&#")) && eat_xint(&x, &code) && code < 0x110000) {
		eatc(&x, ';');
		*xp = x;
		return buf_add_utf8(code, buf);
	}
	return 0;
}

/* Returns the span of a quoted attribute value, includes quotes */
static struct xml
eat_attvalue(struct xml *xp)
{
	if (xp->len < 2)
		return XML_INIT;
	char quote = *xp->s;
	if (quote != '"' && quote != '\'')
		return XML_INIT;
	const char *end = memchr(xp->s + 1, quote, xp->len - 1);
	size_t skip = end ? end - xp->s + 1: xp->len;
	struct xml attvalue = { .s = xp->s, .len = skip };
	eatn(xp, skip);
	return attvalue;
}

/* Return XML with quotes trimmed */
static struct xml
trim_quotes(struct xml x)
{
	if (x.len) {
		char quote = *x.s;
		x.s++;
		x.len--;
		if (x.len && x.s[x.len - 1] == quote)
			x.len--;
	}
	return x;
}


/* Eats <?processing-instruction?> */
static bool
eat_pi(struct xml *xp)
{
	if (!eat(xp, XML("<?")))
		return false;
	eat_until(xp, XML("?>"));
	return true;
}

/* Eats (skips) <tag attr=val...> or <tag/>.
 * Also eats <?processing?>
 * Sets optional *empty_return to indicate empty </tag> */
static bool
eat_tag(struct xml *xp, bool *empty_return)
{
	bool empty = false;
	if (xp->len < 3 || xp->s[1] == '/' || xp->s[1] == '?')
		return false;
	if (!eatc(xp, '<'))
		return false;;
	eat_name(xp);
	eat_space(xp);
	while (xp->len && !eatc(xp, '>')) {
		if (eat(xp, XML("/>"))) {
			empty = true;
			break;
		}
		eat_name(xp);
		eat_space(xp);
		eatc(xp, '=');
		eat_space(xp);
		eat_attvalue(xp);
		eat_space(xp);
	}
	if (empty_return)
		*empty_return = empty;
	return true;
}

/* Eats closing </tag> */
static bool
eat_close(struct xml *xp)
{
	if (!eat(xp, XML("</")))
		return false;
	eat_name(xp);
	eat_space(xp);
	eatc(xp, '>');
	return true;
}

/* Eats text including <![CDATA[..]]>, &entities; and <!--comments--> and
 * stores decoded text into buf.
 * Stops when it encounters an <open> </close> or <empty/> tag.
 * Returns the number of bytes that it tried to add to buf.
 */
static size_t
eat_text(struct xml *xp, struct buf *buf)
{
	size_t ret = 0;
	while (xp->len) {
		if (eat(xp, XML("<!--"))) {
			eat_until(xp, XML("-->"));
			continue;
		}
		if (eat(xp, XML("<![CDATA["))) {
			const char *start = xp->s;
			const char *end = xp->s + xp->len;
			if (eat_until(xp, XML("]]>")))
				end = xp->s - 3;
			ret += buf_addn(start, end - start, buf);
			continue;
		}
		size_t n;
		if ((n = eat_entity(xp, buf))) {
			ret += n;
			continue;
		}
		if ((n = eat_cr(xp, buf))) {
			ret += n;
			continue;
		}
		const char *end = memchrs(xp->s, "<&\r", xp->len);
		size_t len = end ? end - xp->s : xp->len;
		if (!len)
			break; /* found <tag> */
		ret += buf_addn(xp->s, len, buf);
		eatn(xp, len);
	}
	return ret;
}

struct xml
xml_root(struct xml doc)
{
	struct xml i = XML_INIT;
	xml_next_tag(doc, &i);
	return i;
}

/* Return content span of a tag.
 * That is the input should be <tag>content</tag>. */
struct xml
xml_content(struct xml tag)
{
	struct xml content = tag;
	bool empty;
	eat_tag(&content, &empty);
	if (empty)
		return XML_INIT;

	/* Skip balanced <open> and </close> tags */
	unsigned int depth = 1;
	struct xml rest = content;
	while (depth && rest.len) {
		eat_text(&rest, NULL);
		if (eat_pi(&rest))
			continue;
		if (depth == 1) {
			const char *last = rest.s;
			if (eat_close(&rest)) {
				content.len = last - content.s;
				break;
			}
		} else if (eat_close(&rest)) {
			--depth;
			continue;
		}
		if (eat_tag(&rest, &empty) && !empty)
			depth++;
	}
	return content;
}

/* Advances *tag to the next sibling tag within doc.
 * If *tag == XML_INIT then it is advanced to the first tag in doc.
 * Returns false if there are no more tags within doc. */
bool
xml_next_tag(struct xml doc, struct xml *tag)
{
	if (tag->len) {
		/* When continuing, only consider that part of
		 * doc which is after the previously returned tag */
		eatn(&doc, (tag->s + tag->len) - doc.s);
	}

	eat_text(&doc, NULL);
	while (eat_pi(&doc))
		eat_text(&doc, NULL);

	if (!doc.len || eat_close(&doc)) {
		/* end of iteration */
		*tag = XML_INIT;
		return false;
	}
	tag->s = doc.s;
	struct xml content = xml_content(doc);
	const char *end;
	bool empty;
	if (eat_tag(&doc, &empty) && empty) {
		end = doc.s;
	} else {
		/* after = the part of doc that follows content */
		struct xml after = {
			.s = content.s + content.len,
			.len = doc.s + doc.len - (content.s + content.len)
		};
		eat_close(&after);
		end = after.s;
	}
	tag->len = end - tag->s;
	return true;
}

static bool
name_eq(struct xml tag, const char *name, size_t namelen)
{
	if (!eatc(&tag, '<'))
		return false;
	struct xml n = eat_name(&tag);
	return namelen == n.len && memcmp(n.s, name, namelen) == 0;
}

/* Tests if the current tag has a matching name */
bool
xml_name_eq(struct xml tag, const char *name)
{
	return name_eq(tag, name, strlen(name));
}

/* Returns heap-allocated, name of the current tag.
 * Returns NULL (EINVAL) if not a tag. */
char *
xml_get_name(struct xml tag)
{
	if (!eatc(&tag, '<'))
		return errno = EINVAL, NULL;

	struct xml n = eat_name(&tag);
	if (!n.len)
		return errno = EINVAL, NULL;

	struct buf b = BUF_INIT;
	buf_addn(n.s, n.len, &b);
	return buf_return(&b, NULL);
}

/* Finds the span for an attribute's value, excluding quotes.
 * Returns false if not found.  */
static bool
find_attr(struct xml tag, const char *name, int namelen, struct xml *ret)
{
	if (!namelen)
		return false;
	if (!eatc(&tag, '<'))
		return false;
	eat_name(&tag);
	eat_space(&tag);
	while (tag.len && *tag.s != '/' && *tag.s != '>') {
		struct xml attrname = eat_name(&tag);
		eat_space(&tag);
		eatc(&tag, '=');
		eat_space(&tag);
		struct xml value = eat_attvalue(&tag);
		if (attrname.len == namelen &&
		    memcmp(name, attrname.s, namelen) == 0)
		{
			/* Remove quotes */
			*ret = trim_quotes(value);
			return true;
		}
		eat_space(&tag);
	}
	return false;
}

/* Returns heap-allocated, decoded value of named attribute value.
 * Returns NULL (ENOENT) if not found. */
char *
xml_get_attr(struct xml tag, const char *name, size_t *len_return)
{
	struct xml value;
	if (!find_attr(tag, name, strlen(name), &value))
		return errno = ENOENT, NULL;

	/* Decode, assuming it contains no '<' */
	struct buf b = BUF_INIT;
	eat_text(&value, &b);

	return buf_return(&b, len_return);
}

bool
xml_attr_eq(struct xml tag, const char *attr, const char *value)
{
	struct xml attval;
	if (!find_attr(tag, attr, strlen(attr), &attval))
		return !value;
	if (!value)
		return false;

	int valuelen = strlen(value);
	char tmp[valuelen + 1];
	struct buf b = BUF_FIXED(tmp, sizeof tmp);
	size_t len = eat_text(&attval, &b);

	return !b.error && len == valuelen && memcmp(tmp, value, len) == 0;
}

/* Returns heap-allocated, decoded text data.
 * It is OK to use this function on attribute values because
 * well-formed XML attributes do not contain '<'. */
char *
xml_get_text(struct xml doc, size_t *len_return)
{
	struct buf b = BUF_INIT;
	while (doc.len) {
		eat_text(&doc, &b);
		if (eat_pi(&doc))
			continue;
		if (!eat_close(&doc))
			eat_tag(&doc, NULL);
	}
	return buf_return(&b, len_return);
}

/* Finds the XML path.
 * Returns true on success, and sets *ret to the span.
 * Returns false (EINVAL) if the path is invalid.
 * Returns false (ENOENT) if the path was not found */
static bool
find_at(struct xml doc, const char *path, struct xml *ret)
{
again:
	if (*path == '/')
		++path;

	const char *name = path;
	size_t namelen = strcspn(path, "/[@");
	if (!namelen)
		return errno = EINVAL, false;
	path += namelen;

	/* Extract optional [index] */
	unsigned index = 1;
	if (*path == '[') {
		index = 0;
		while (*++path != ']' && *path) {
			if (!isdigit(*path))
				return errno = EINVAL, false;
			index = index * 10 + *path - '0';
		}
		if (!*path++ || index < 1)
			return errno = EINVAL, false;
	}

	/* Find the named element */
	struct xml iter = XML_INIT;
	while (index) {
		if (!xml_next_tag(doc, &iter))
			return errno = ENOENT, false;
		if (name_eq(iter, name, namelen))
			index--;
	}

	doc = iter;
	if (*path == '@') {
		path++;
		if (!find_attr(doc, path, strlen(path), &doc))
			return errno = ENOENT, false;
		path = "";
	}
	if (*path == '/') {
		path++;
		doc = xml_content(doc);
	}
	if (*path)
		goto again;

	*ret = doc;
	return true;
}

struct xml
xml_at(struct xml xml, const char *path)
{
	struct xml ret;
	if (!find_at(xml, path, &ret))
		return XML_INIT;
	return errno = 0, ret;
}

char *
xml_get_at(struct xml xml, const char *path, size_t *len_return)
{
	struct xml ret;
	if (!find_at(xml, path, &ret))
		return NULL;
	return xml_get_text(ret, len_return);
}

bool
xml_next_attr(struct xml tag, struct xml *aiter)
{
	if (aiter->len) {
		/* Eat tag up to and including *aiter */
		eatn(&tag, (aiter->s + aiter->len) - tag.s);
	} else {
		eatc(&tag, '<');
		eat_name(&tag);
	}

	eat_space(&tag);
	if (!tag.len || *tag.s == '/' || *tag.s == '>') {
		aiter->s = NULL;
		aiter->len = 0;
		return false;
	}
	const char *start = tag.s;
	eat_name(&tag);
	eat_space(&tag);
	eatc(&tag, '=');
	eat_space(&tag);
	eat_attvalue(&tag);
	aiter->s = start;
	aiter->len = tag.s - start;
	return true;
}

char *
xml_attr_get_name(struct xml attr)
{
	struct xml name = eat_name(&attr);
	struct buf b = BUF_INIT;
	buf_addn(name.s, name.len, &b);
	return buf_return(&b, NULL);
}

char *
xml_attr_get_value(struct xml attr, size_t *len_return)
{
	eat_name(&attr);
	eat_space(&attr);
	eatc(&attr, '=');
	eat_space(&attr);
	struct xml value = eat_attvalue(&attr);
	value = trim_quotes(value);
	return xml_get_text(value, len_return);
}
