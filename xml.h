#pragma once
#include <stdbool.h>
#include <stdlib.h>

/**
 * A span of XML input.
 * UTF-8 is assumed.
 */
struct xml { const char *s; size_t len; };

#define XML_INIT (struct xml){ 0, 0 }

/** Returns the root element of the document. */
struct xml xml_root(struct xml doc);

/** Returns the content XML of an element. */
struct xml xml_content(struct xml tag);

/**
 * Accesses part of an XML document using an XPath-like path.
 *
 * For example, if the document is:
 * @code{.xml}
 *      <a name="top">
 *        <b>Example</b>
 *        <b>
 *           <c>Something</c>
 *        </b>
 *      </a>
 * @endcode
 * Then the following paths select:
 * <ul>
 *  <li> a/b[2]/c  -> "<c>Something</c>"
 *  <li> a/b[2]/c/ -> "Something"
 *  <li> a@name    -> "top"
 * </ul>
 *
 * @param xml  An XML span to search.
 * @param path The path to locate.
 *             A path is a sequence of slash-delimited tag names,
 *             optionally terminated by a trailing slash, or 
 *             an '@' sign followed by an attribute name.
 *             Leading slashes are ignored.
 *             Tag names may be followed by a 1-based index number in square
 *             brackets, defaulting to 1.
 * @returns The span within the XML correspoding to the path.
 *          Use #xml_get_text() to obtain decoded text.
 * @returns #XML_INIT if the path didn't match anything.
 * @returns #XML_INIT if the path was malformed.
 */
struct xml xml_at(struct xml xml, const char *path);


/**
 * Advances an iterator to the next sibling element.
 *
 * First, initialise @a iter to #XML_INIT.
 * After the first call, @a iter will be set to the span of the first
 * element found in the @a xml span, and this function will return true.
 * The @a iter span will cover the entire first element, including its
 * open tag, content and closing tag.
 *
 * On the second call, @a iter will be advanced to the sibling-wise next
 * element, skipping any intermediate text, and the function will return true.
 *
 * This continues until the function returns false.
 *
 * @param xml  Span of XML to iterate over.
 * @param iter Pointer to iterator storage. The storage should be
 *             initialised to #XML_INIT and must be left unchanged
 *             between subsequent calls. It will be updated to the
 *             spans of each top-level sibling element within #xml.
 * @returns true iff #iter was set to the next element.
 * @returns false iff no more elements and #iter was set to #XML_INIT.
 */
bool xml_next_tag(struct xml xml, struct xml *iter);

/**
 * Advances an iterator to the next attribute.
 *
 * First, initialise @a aiter to #XML_INIT.
 * After the first call, @a aiter will be set to the span of the first
 * attribute found in the @a tag span, and this function will return true.
 *
 * On the second call, @a iter will be advanced to the sibling-wise next
 * element, skipping any intermediate text, and the function will return true.
 *
 * This continues until the function returns false.
 *
 * @param tag   Span of an XML tag to iterate over.
 * @param aiter Pointer to iterator storage. The storage should be
 *              initialised to #XML_INIT and must be left unchanged
 *              between subsequent calls. It will be updated to the
 *              spans of each attribute within #tag.
 * @returns true iff #iter was set to the next element.
 * @returns false iff no more elements and #iter was set to #XML_INIT.
 */
bool xml_next_attr(struct xml tag, struct xml *aiter);

/** Extracts the name of the attribute span. */
char *xml_attr_get_name(struct xml attr);

/** Extracts the value of the attribute span. */
char *xml_attr_get_value(struct xml attr, size_t *len_return);

/** Tests the tag's name for equality with the given string. */
bool xml_name_eq(struct xml tag, const char *name);

/**
 * Tests the tag's attribute's value for equality with the given string.
 * @param tag   Span of XML that begins with an opening or empty tag.
 * @param name  The UTF-8 name of the attribute to access.
 * @param value A UTF-8 value to compare against, or NULL to
 *              test that the attribute is missing. The value cannot 
 *              cannot NUL (use #xml_get_attr() instead).
 * @returns true if the attribute has an equal value
 */
bool xml_attr_eq(struct xml tag, const char *attr, const char *value);

/**
 * Returns a copy of the tag's name.
 * @param tag  Span of XML that begins with an opening or empty tag.
 * @returns a heap-allocated string that should be released with #free().
 */
char *xml_get_name(struct xml tag);

/**
 * Returns the tag's attribute's value.
 * @param tag Span that begins with the XML element holding the attribute.
 * @param name The UTF-8 name of the attribute to access.
 * @param len_return Optional storage for returning the length of
 *                   the string returned. Attributes may contain NULs.
 * @returns Copy of the named attribute's decoded value, to be
 *          released with #free().
 * @returns NULL if the attribute was not found.
 */
char *xml_get_attr(struct xml tag, const char *name, size_t *len_return);

/**
 * Returns a copy of the text data.
 * Text data is any data that is not tags, comments, or processing
 * instructions.
 * For example, if the input XML string is
 * \code "That&apos;s <b><i>good</i></b>!" \endcode
 * then the outpt string will be
 * \code "That's good!" \endcode .
 * @note CR and CRLF sequences are converted to LF.
 * @note the returns data may contain NULs
 *
 * @param xml Span of XML
 * @param len_return optional storage for returning the text size.
 * @returns Copy of the decoded text data, to be released with #free().
 */
char *xml_get_text(struct xml xml, size_t *len_return);

/**
 * Accesses the text value of a path.
 * @param xml  The XML to search.
 * @param path Path to the value. See #xml_at() for details.
 * @param len_return optional storage for returning the text size.
 * @returns Text value of the element located. Release with #free().
 * @returns NULL if the path wasn't found.
 */
char *xml_get_at(struct xml xml, const char *path, size_t *len_return);
