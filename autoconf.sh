#!/bin/sh
rm -rf autom4te.cache m4
rm -f Makefile.in aclocal.m4 aminclude.am ar-lib
rm -f compile config.guess config.sub configure depcomp install-sh ltmain.sh
rm -f missing test-driver

mkdir -p m4
autoreconf --install --verbose
